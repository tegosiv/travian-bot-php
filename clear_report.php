<?php

use Application\Common\Env;
use Application\Travian\Game;
use Application\Utils\Log;
use Application\Utils\RandomBreak;

$start = microtime(true);
require 'vendor/autoload.php';

define('APP_ROOT', __DIR__);

new Env(__DIR__);

$tag = Game::$tag;

Log::i($tag, '---------------------');
Log::i($tag, 'Cron start');
Log::i($tag, 'Action - Reports');

try {
	$game = new Game();

	$game_server_date = $game->getServerDate();
	$hours = (int)$game_server_date->format('H');

	Log::i($tag, 'Game server time: ' . $game_server_date->format('d.m.Y H:i:s'));

	// auth
	$auth = $game->makeAuth();

	$probability = .1;
	$rand = RandomBreak::getRand();
	$probability += ($rand + (rand(5, 30) / 100));
	Log::i($tag, 'Probability: ' . $probability);

	RandomBreak::makeBreak($probability);

	// random sleep
	RandomBreak::randomSleep(5, 50);

	if ($auth) {
		$game->makeRandomActions();

		$total_messages = 0;
		$total_messages += $game->clearOffensiveReport();
		$total_messages += $game->clearOffensiveReport();
		$total_messages += $game->clearMerchantsReport();

		Log::i($tag, 'Messages : ' . $total_messages . ' removed');
	}
} catch (Exception $e) {
	Log::w($tag, 'Message: ' . $e->getMessage());
}


Log::i($tag, 'Execute time: ' . round(microtime(true) - $start, 4) . ' sec.');