<?php

use Application\Exceptions\RandomBreakException;
use Application\Utils\Log;
use Application\Utils\RandomBreak;

$start = microtime(true);
require 'vendor/autoload.php';

define('APP_ROOT', __DIR__);

new Application\Common\Env(__DIR__);

$tag = Application\Travian\Game::$tag;

Application\Utils\Log::i($tag, '---------- Action - Farm ----------');

try {
	$game = new Application\Travian\Game();

	$game_server_date = $game->getServerDate();
	$hours = (int)$game_server_date->format('H');
	$probability = 0.0;

	if ($hours > 0 && $hours < 5) {
		$probability -= 0.1;
	}

	Application\Utils\Log::i($tag, 'Game server time: ' . $game_server_date->format('d.m.Y H:i:s'));

	// auth
	$auth = $game->makeAuth();

	$rand = RandomBreak::getRand();

	$little_rand = (rand(5, 30) / 100);
	$probability += ($rand + $little_rand);

	Application\Utils\Log::i($tag, 'Probability: ' . $probability);

	$runs = Application\Utils\Helper::getTotalRuns();

	try {
		RandomBreak::makeBreak($probability, .2);

		if ($runs > 7) {
			Application\Utils\Helper::setTotalRuns(0);
			throw new Exception('Force break');
		}

		// random sleep
		RandomBreak::randomSleep(10, 50);

		if ($auth) {
			$game->runFarmLists();
			$runs++;
			Application\Utils\Helper::setTotalRuns($runs);
		}
	} catch (RandomBreakException $exception) {
		if ($runs > 7) {
			Application\Utils\Helper::setTotalRuns(0);
			throw new Exception('Force break');
		}
	}

} catch (Exception $e) {
	Log::w($tag, 'Message: ' . $e->getMessage());
}

Log::i($tag, 'Execute time: ' . round(microtime(true) - $start, 4) . ' sec.');