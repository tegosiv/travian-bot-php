<?php

use Application\Common\Env;
use Application\Travian\Game;
use Application\Utils\Log;

$start = microtime(true);
require 'vendor/autoload.php';

define('APP_ROOT', __DIR__);

new Env(__DIR__);

$tag = Game::$tag;

Log::i($tag, '---------------------');
Log::i($tag, 'Cron start');
Log::i($tag, 'Action - Notify Auction');

try {
	$game = new Game();

	$game_server_date = $game->getServerDate();
	$hours = (int)$game_server_date->format('H');

	Log::i($tag, 'Game server time: ' . $game_server_date->format('d.m.Y H:i:s'));

	// auth
	$auth = $game->makeAuth();

	if ($auth) {
		echo $game->notifySellingAuction();
	}

} catch (Exception $e) {
	Log::w($tag, 'Message: ' . $e->getMessage());
}

Log::i($tag, 'Execute time: ' . round(microtime(true) - $start, 4) . ' sec.');