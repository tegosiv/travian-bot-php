<?php

use Application\Common\Env;
use Application\Travian\Game;
use Application\Utils\Log;
use Application\Utils\RandomBreak;

$start = microtime(true);
require 'vendor/autoload.php';

define('APP_ROOT', __DIR__);

new Env(__DIR__);

$tag = Game::$tag;

Log::i($tag, '----------- Action - Auction ----------');

try {
	$game = new Game();

	$game_server_date = $game->getServerDate();
	$hours = (int)$game_server_date->format('H');

	Log::i($tag, 'Game server time: ' . $game_server_date->format('d.m.Y H:i:s'));

	// auth
	$auth = $game->makeAuth();

	$rand = RandomBreak::getRand();
	Log::i($tag, 'Probability: ' . $rand);

	RandomBreak::makeBreak($rand, .3);

	// random sleep
	RandomBreak::randomSleep(5, 30);

	if ($auth) {
		$bids = $game->makeBids();
		Log::i($tag, "{$bids} bids.");
		$cleared = $game->clearFinishedAuctions();
		Log::i($tag, "{$cleared} finished bids removed.");
	}

} catch (Exception $e) {
	Log::w($tag, 'Message: ' . $e->getMessage());
}

Log::i($tag, 'Execute time: ' . round(microtime(true) - $start, 4) . ' sec.');