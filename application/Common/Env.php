<?php

namespace Application\Common;

class Env
{
	public function __construct($dir)
	{
		$dotenv = \Dotenv\Dotenv::create($dir);
		$dotenv->load();
	}
}