<?php

namespace Application\Travian;

use Application\Travian\Auction\AuctionItem;
use Application\Utils\Helper;
use Application\Utils\HtmlTable;
use Application\Utils\Log;
use Application\Utils\RandomBreak;
use Application\Utils\SendMail;
use Carbon\Carbon;
use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Message;
use GuzzleHttp\TransferStats;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

final class Game
{
	private $client;

	public static $tag = "TRAVIAN - CRON";

	protected $ajaxToken = '';

	protected $baseUrl = '';

	private $cookieFile;

	public function __construct()
	{
		$this->baseUrl = getenv('TRAVIAN_SERVER');
		$this->cookieFile = APP_ROOT . '/data/cookie_jar.txt';

		$cookieJar = new FileCookieJar($this->cookieFile, true);

		$this->client = new Client(
			[
				'base_uri' => $this->baseUrl,
				'cookies' => $cookieJar,
				'headers' => [
					'User-Agent' => getenv('USER_AGENT'),
					'Accept' => '*/*',
					'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,uk;q=0.2',
				],
			]
		);
	}

	public function makeAuth()
	{
		$method = 'GET';
		$response = $this->client->get('/dorf1.php');

		$response_content = $response->getBody()->getContents();
		// cookie ends, need login
		if (strpos($response_content, 'login.php') !== false) {

			$method = 'POST';
			$response = $this->client->post('/login.php',
				[
					'form_params' => [
						'name' => getenv('TRAVIAN_LOGIN'),
						'password' => getenv('TRAVIAN_PASSWORD'),
						's1' => 'Login',
						'w' => '1536:864',
						'login' => time()
					]
				]
			);

			$response_content = $response->getBody()->getContents();
		}

		$this->setAjaxToken($response_content);


		Log::i(self::$tag, 'Auth method: ' . $method);
		Log::i(self::$tag, 'ajaxToken: ' . $this->ajaxToken);

		return !empty($this->ajaxToken);
	}

	/**
	 * @param $response_content
	 */
	private function setAjaxToken($response_content)
	{
		// set ajaxToken
		$html = $response_content;
		$crawler = new Crawler($html);

		$scripts = $crawler->filter('script')
			->reduce(function (Crawler $node) {
				return strpos($node->text(), 'eval') !== false;
			});

		$eval_script = $scripts->first()->text();

		$lines = array_filter(explode(';', $eval_script));

		$eval_string = '';
		foreach ($lines as $line) {
			if (strpos($line, 'eval') !== false) {
				$eval_string = $line;
			}
		}

		$regex = "/.*\(([^)]*)\)/";
		preg_match($regex, $eval_string, $matches);

		$atob_content = end($matches);
		$atob_content = str_replace("'", '', $atob_content);
		$content = base64_decode($atob_content);

		$parts = explode('&&', $content);
		$token = trim($parts[1] ?? '');
		$token = str_replace("'", '', $token);

		$this->ajaxToken = $token;

	}

	protected function makeRequest($request_data, $debug = false): ResponseInterface
	{
		sleep(rand(1, 7));

		$option = [];
		$headers = $request_data['headers'] ?? [];

		if ($request_data['bearer'] ?? true) {
			$option['headers']['Authorization'] = 'Bearer ' . $this->ajaxToken;
		}

		if ($request_data['ajax'] ?? false) {
			$option['headers']['X-Requested-With'] = 'XMLHttpRequest';
		}

		$option['headers'] = array_merge($headers, $option['headers']);

		if ($debug) {
			$option['on_stats'] = function (TransferStats $stats) {
				echo 'Debug request:';
				echo $stats->getEffectiveUri() . "\n";
				echo $stats->getTransferTime() . "\n";
				var_dump($stats->getHandlerStats());
				var_dump(Message::toString($stats->getRequest()));

				// response object.
				if (!$stats->hasResponse()) {
					var_dump($stats->getHandlerErrorData());
				}
			};
		}

		if (isset($request_data['body'])) {
			$option['form_params'] = $request_data['body'];
		}

		if (isset($request_data['json'])) {
			$option['json'] = $request_data['json'];
		}

		return $this->client->request(
			$request_data['method'],
			$request_data['url'],
			$option
		);
	}

	public function runFarmLists()
	{
		$rallyPointFarmList = $this->client->get('/build.php?tt=99&id=39');
		$crawler = new Crawler($rallyPointFarmList->getBody()->getContents());
		$raidList = $crawler->filter('#raidList .raidList');

		$sort_params = [
			[
				'field' => 'lastRaid',
				'direction' => 'desc'
			],
			[
				'field' => 'lastRaid',
				'direction' => 'asc'
			],
			[
				'field' => 'distance',
				'direction' => 'asc'
			],
			[
				'field' => 'distance',
				'direction' => 'desc'
			],
			[
				'field' => 'bounty',
				'direction' => 'desc'
			],
			[
				'field' => 'ew',
				'direction' => 'asc'
			],
			[
				'field' => 'ew',
				'direction' => 'desc'
			],
		];

		$sort_param = $sort_params[array_rand($sort_params)];


		Log::i(self::$tag, 'Sort: ' . json_encode($sort_param));

		if ($raidList->count() > 0) {
			$allowed_list_ids = Helper::getAllowedFarmList();
			Log::i(self::$tag, 'Farm lists: ' . json_encode($allowed_list_ids));

			$first_run = true;
			$raidListArray = [];

			foreach ($raidList as $list) {
				$element = new Crawler($list);
				$raidListArray[] = $element;
			}

			$rand = RandomBreak::getRand();
			if ($rand > .5) {
				shuffle($raidListArray);
			}

			foreach ($raidListArray as $element) {

				if ($first_run) {
					$param_a = $element->filter("form input[name=a]")->attr('value');
				}

				$raid_list_id = $element->attr('data-listid');
				if (in_array($raid_list_id, $allowed_list_ids)) {
					$raid_form_data = [
						'method' => 'ActionStartRaid',
						'listId' => $raid_list_id,
						'slots' => [],
						'sort' => $sort_param['field'],
						'direction' => $sort_param['field'],
						'captcha' => null,
						'a' => $param_a ?? '',
						'loadedLists' => [],
					];

					$json = $this->runFarmList($raid_form_data);
					$param_a = $json['a'];
					$first_run = false;
				}
			}
		}
	}

	public function runFarmList($raidData)
	{
		$response = $this->makeRequest(
			[
				'method' => 'post',
				'url' => '/api/v1/raid-list',
				'json' => $raidData,
				'ajax' => true
			]
		);

		return json_decode($response->getBody()->getContents(), true);
	}

	public function clearOffensiveReport(): int
	{
		// offensive - without losses
		$url_report = '/report/offensive?opt=AAABAA==';
		return $this->clearReport($url_report);
	}

	public function clearMerchantsReport(): int
	{
		// merchants
		$url_report = '/report/other?opt=AAALAAwADQAOAA==';
		return $this->clearReport($url_report);
	}

	public function clearReport($url_report): int
	{
		$total_messages = 0;

		$reportsPage = $this->makeRequest(
			[
				'method' => 'get',
				'url' => $url_report
			]
		);

		$crawler = new Crawler($reportsPage->getBody()->getContents());
		$inputs = $crawler->filter('#reportsForm table tr td.sel input');

		$input_array = [];

		foreach ($inputs as $node) {
			$element = new Crawler($node);
			$id = $element->attr('value');
			$input_array[] = $id;
		}

		$total_inputs = count($input_array);
		$total_messages += $total_inputs;


		if ($total_inputs > 0) {
			$post_data = [
				'ids' => $input_array,
				'do' => 'delete',
			];

			$this->makeRequest([
				'method' => 'post',
				'url' => '/report/offensive?page=1',
				'body' => $post_data
			], true);
		}

		return $total_messages;
	}

	public function getServerDate(): DateTime
	{
		return Carbon::now(getenv('TIME_ZONE'));
	}

	public function getAuctionData(): array
	{
		$auction_url = '/hero/auction?action=sell';

		$auction_page = $this->makeRequest(
			[
				'method' => 'get',
				'url' => $auction_url
			]
		);

		$crawler = new Crawler($auction_page->getBody()->getContents());
		$scripts = $crawler->filter('script')
			->reduce(function (Crawler $node) {
				return strpos($node->text(), 'checkSum') !== false;
			});

		$check_sum_script = $scripts->first()->html();


		$lines = explode("\n", str_replace(["\r\n", "\n\r", "\r"], "\n", $check_sum_script));


		$check_sum_string = '';
		foreach ($lines as $line) {
			if (strpos($line, 'checkSum') !== false) {
				$check_sum_string = $line;
			}
		}

		$check_sum_string = trim($check_sum_string);
		$check_sum_string = rtrim($check_sum_string, ', {');
		$check_sum_string = rtrim($check_sum_string, '}');
		$check_sum_string = str_replace('data:', '', $check_sum_string);
		$check_sum_string = trim($check_sum_string);

		$result = [];
		$data = json_decode($check_sum_string, true);
		if (!empty($data)) {
			$result = $data ?? [];
		}

		return $result;
	}

	public function makeBids($page = 1, $total_made_bids = 0): int
	{
		$auction_api_data_url = '/api/v1/hero/auction/data';
		$auction_data = $this->makeRequest([
			'method' => 'post',
			'url' => $auction_api_data_url,
			'json' => [
				'dataType' => 'runningAuctions',
				'page' => $page
			],
			'ajax' => true
		]);

		Log::i(self::$tag, 'Page: ' . $page);

		$auction_js_data = $this->getAuctionData();
		$auction_common_data = $auction_js_data['common'];
		$auction_sell_data = $auction_js_data['sell'];
		$selling = $auction_sell_data['currentlySelling'];

		$selling_identifiers = array_column($selling, 'identifier');

		$silver_amount = $auction_common_data['silver'] ?? 0;
		$z_param = $auction_common_data['checkSum'] ?? '';

		$json_array_auctions = json_decode($auction_data->getBody()->getContents(), true)['data'] ?? [];

		$auction_bid_url = '/api/v1/hero/auction/bid';

		if (count($json_array_auctions) > 0 && !empty($z_param)) {

			$ignored_item_ids = AuctionItem::getIgnoredItemIds();

			// help bid
			$this->makeRequest([
				'method' => 'get',
				'url' => '/api/v1/hero/auction/bid-help',
				'ajax' => true,
			]);

			foreach ($json_array_auctions as $array_auction) {
				// skip ignored
				if (in_array($array_auction['item_type_id'], $ignored_item_ids)) continue;

				$coefficient = AuctionItem::getCoefficient($array_auction['identifier'], $selling_identifiers);

				$price = AuctionItem::getPrice($array_auction);
				$price = $price * $array_auction['amount'];
				$price = round($price + ($price * rand(-5, 7) / 100)) * $coefficient;

				if ($silver_amount > $price && $price > $array_auction['minBid'] && !$array_auction['currentBidder']) {

					$post_data = [
						'amount' => $array_auction['amount'],
						'auctionId' => $array_auction['identifier'],
						'maxBid' => $price,
						'buyTabFilter' => $array_auction['category'],
						'page' => 1,
						'a' => 'auctions',
						'z' => $z_param
					];

					try {
						$auction_request_bid = $this->makeRequest([
							'method' => 'post',
							'url' => $auction_bid_url,
							'json' => $post_data,
							'ajax' => true,
						]);

						$auction_request_bid_response_json = json_decode($auction_request_bid->getBody()->getContents(), true);
						$z_param = $auction_request_bid_response_json['z'];
						$total_made_bids++;

					} catch (RequestException $e) {
						if ($e->hasResponse()) {
							$response = json_decode($e->getResponse()->getBody()->getContents(), true);
							$z_param = $response['z'];
							if ($total_made_bids > 0) {
								$total_made_bids--;
							}
						}
					}
				}
			}
		}

		if ($page < 2) {
			$page++;
			$this->makeBids($page, $total_made_bids);
		}

		return $total_made_bids;
	}

	public function clearFinishedAuctions($page = 1, $total_cleared = 0): int
	{
		$auction_api_data_url = '/api/v1/hero/auction/data';
		$auction_finished_data = $this->makeRequest([
			'method' => 'post',
			'url' => $auction_api_data_url,
			'json' => [
				'dataType' => 'finishedBids',
				'finishedBidsPage' => $page
			],
			'ajax' => true
		]);

		Log::i(self::$tag, 'Page: ' . $page);

		$json_array_auctions = json_decode($auction_finished_data->getBody()->getContents(), true)['data'] ?? [];

		$finished_not_current_bidder = array_filter(
			$json_array_auctions,
			function ($data) {
				return !$data['currentBidder'];
			}
		);

		$auction_for_delete_ids = array_column($finished_not_current_bidder, 'id');

		$auction_bid_url = '/api/v1/hero/auction';

		if (count($auction_for_delete_ids) > 0) {
			$post_data = [
				'delete' => $auction_for_delete_ids,
				'status' => 'finished'
			];

			$auction_request_delete = $this->makeRequest([
				'method' => 'delete',
				'url' => $auction_bid_url,
				'json' => $post_data,
				'ajax' => true,
			]);


			$total_cleared += count($auction_for_delete_ids);
		}

		if ($page < 1) {
			$page++;
			$this->clearFinishedAuctions($page, $total_cleared);
		}

		return $total_cleared;
	}

	public function notifySellingAuction(): string
	{
		$auction_js_data = $this->getAuctionData();
		$auction_sell_data = $auction_js_data['sell'];
		$selling = $auction_sell_data['currentlySelling'];

		$game_server_date = $this->getServerDate();

		foreach ($selling as $k => $item) {
			unset($selling[$k]['description']);
			unset($selling[$k]['couldBeDeleted']);
			unset($selling[$k]['identifier']);
			unset($selling[$k]['obfuscatedId']);

			$date_end = Carbon::createFromTimestamp($item['time_end'])->timezone(getenv('TIME_ZONE'));
			$date_end_local = Carbon::createFromTimestamp($item['time_end'])->timezone(getenv('TIME_ZONE_LOCAL'));
			$selling[$k]['time_end_date'] = $date_end->format('d.m.Y H:i:s');
			$selling[$k]['left_time'] = $date_end->diff($game_server_date)->format('%H:%I:%S');

			$selling[$k]['time_end_date_local'] = $date_end_local->format('d.m.Y H:i:s');

		}

		if (count($selling) > 0) {
			$table = HtmlTable::build($selling);

			SendMail::send([
				'subject' => 'Selling Auction',
				'body' => $table,
				'is_body_html' => true,
			]);

			return $table;
		}
		return '';
	}

	public function getVillageName($village_id = 0): string
	{
		$response = $this->client->get('/dorf1.php?newdid=' . $village_id);
		$response_content = $response->getBody()->getContents();
		$crawler = new Crawler($response_content);
		return $crawler->filter('#villageName .villageInput')->attr('value');
	}

	public function changeVillageName($village_id = 0, $name = '')
	{
		$village_api_url = '/api/v1/village/change-name';

		$village_data = $this->makeRequest([
			'method' => 'post',
			'url' => $village_api_url,
			'json' => [
				'name' => $name,
				'did' => $village_id
			],
			'ajax' => true
		]);

		$resp = $village_data->getBody()->getContents();

		Log::i(self::$tag, $resp);
	}

	public function makeRandomActions(): int
	{
		$kRuns = 0;

		$actions = Helper::getGameAction();
		$count = count($actions);
		$min = 1;
		$max = 4;
		if ($max > $count) {
			$max = $count;
		}
		$num = rand($min, $max);

		$result = [];
		$randomAction = array_rand($actions, $num);
		if (!is_array($randomAction)) {
			$randomAction = [$randomAction];
		}
		foreach ($randomAction as $k) {
			$result[] = $actions[$k];
		}

		if (count($result)) {
			foreach ($result as $link) {
				$this->makeRequest([
					'method' => 'get',
					'url' => $link
				]);
				$kRuns++;
			}
		}

		return $kRuns;
	}

}