<?php

namespace Application\Utils;

use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

final class SendMail
{
	public static function send($data = ['subject' => 'Subject', 'body' => 'Body', 'is_body_html' => true]): int
	{
		try {


			$transport = (new Swift_SmtpTransport(getenv('MAIL_HOST'), getenv('MAIL_PORT'), getenv('MAIL_ENCRYPTION')))
				->setUsername(getenv('MAIL_USERNAME'))
				->setPassword(getenv('MAIL_PASSWORD'));

			$mailer = new Swift_Mailer($transport);

			// Create a message
			$message = (new Swift_Message($data['subject']))
				->setFrom([getenv('MAIL_USERNAME') => 'Travian'])
				->setTo([getenv('MAIL_TO')])
				->setBody($data['body']);

			if ($data['is_body_html'] ?? false) {
				$message->setContentType('text/html');
			}

			// Send the message
			return $mailer->send($message);
		} catch (\Exception $e) {
			return false;
		}
	}
}