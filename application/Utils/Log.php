<?php

namespace Application\Utils;

use Exception;

final class Log
{
	/**
	 * @var Log
	 */
	private static $instance;

	private $log_file;

	/**
	 * gets the instance via lazy initialization (created on first usage)
	 */
	public static function getInstance(): Log
	{
		if (is_null(static::$instance)) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * Constructor
	 * @param String $log_file - [optional] Absolute file name/path. Defaults to ubuntu apache log.
	 * @throws Exception
	 */
	private function __construct($log_file = '')
	{
		if (empty($log_file)) {
			$log_file = APP_ROOT . '/data/logs/' . date('Y-m-d') . '.txt';
		}

		$this->log_file = $log_file;

		if (!file_exists($log_file)) { //Attempt to create log file
			touch($log_file);
		}

		//Make sure we'ge got permissions
		if (!(is_writable($log_file))) {
			//Cant write to file,
			throw new Exception("LOGGER ERROR: Can't write to log", 1);
		}
	}

	/**
	 * d - Log Debug
	 * @param String tag - Log Tag
	 * @param String message - message to spit out
	 * @return void
	 **/
	public static function d($tag, $message)
	{
		self::getInstance()->writeToLog("DEBUG", $tag, $message);
	}

	/**
	 * e - Log Error
	 * @param String tag - Log Tag
	 * @param String message - message to spit out
	 * @author
	 **/
	public static function e($tag, $message)
	{
		self::getInstance()->writeToLog("ERROR", $tag, $message);
	}

	/**
	 * w - Log Warning
	 * @param String tag - Log Tag
	 * @param String message - message to spit out
	 * @author
	 **/
	public static function w($tag, $message)
	{
		self::getInstance()->writeToLog("WARNING", $tag, $message);
	}

	/**
	 * i - Log Info
	 * @param String tag - Log Tag
	 * @param String message - message to spit out
	 * @return void
	 **/
	public static function i($tag, $message)
	{
		self::getInstance()->writeToLog("INFO", $tag, $message);
	}

	/**
	 * writeToLog - writes out timestamped message to the log file as
	 * defined by the $log_file class variable.
	 *
	 * @param String status - "INFO"/"DEBUG"/"ERROR" e.t.c.
	 * @param String tag - "Small tag to help find log entries"
	 * @param String message - The message you want to output.
	 * @return void
	 **/
	private function writeToLog($status, $tag, $message)
	{
		$date = date('[Y-m-d H:i:s]');
		$msg = "$date: [$tag][$status] - $message" . PHP_EOL;
		file_put_contents($this->log_file, $msg, FILE_APPEND);
	}

	/**
	 * prevent the instance from being cloned (which would create a second instance of it)
	 */
	private function __clone()
	{
	}

	/**
	 * prevent from being unserialized (which would create a second instance of it)
	 */
	private function __wakeup()
	{
	}
}