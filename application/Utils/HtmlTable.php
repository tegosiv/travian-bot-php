<?php

namespace Application\Utils;

final class HtmlTable
{
	public static function build($array): string
	{
		// start table
		$html = '<table width="100%" border="1">';
		// header row
		$html .= '<tr>';
		foreach ($array[0] as $key => $value) {
			$html .= '<th>' . ($key) . '</th>';
		}
		$html .= '</tr>';

		// data rows
		foreach ($array as $key => $value) {
			$html .= '<tr>';
			foreach ($value as $key2 => $value2) {
				$html .= '<td>' . ($value2) . '</td>';
			}
			$html .= '</tr>';
		}

		// finish table and return it

		$html .= '</table>';
		return $html;
	}
}