<?php

namespace Application\Utils;

use Application\Exceptions\RandomBreakException;
use Application\Travian\Game;

final class RandomBreak
{
	/**
	 * @param float $probability
	 * @param float $check
	 * @throws RandomBreakException
	 */
	public static function makeBreak($probability = .1, $check = 0.4)
	{
		if ($probability < $check) {
			Log::i(Game::$tag, 'Random break');
			throw new RandomBreakException('Random break');
		}
	}

	public static function getRand()
	{
		return (float)rand() / (float)getrandmax();
	}

	public static function randomFloat($min = 0, $max = 1)
	{
		return random_int($min, $max - 1) + (random_int(0, PHP_INT_MAX - 1) / PHP_INT_MAX);
	}

	public static function randomSleep($min = 10, $max = 100)
	{
		var_dump($_SERVER['HTTP_HOST']);
		if ($_SERVER['HTTP_HOST'] !== 'travian.loc') {
			sleep(rand($min, $max));
		} else {
			var_dump('No sleep');
		}
	}
}