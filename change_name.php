<?php

use Application\Common\Env;
use Application\Travian\Game;
use Application\Utils\Log;

$start = microtime(true);
require 'vendor/autoload.php';

define('APP_ROOT', __DIR__);

new Env(__DIR__);

$tag = Game::$tag;

Log::i($tag, '---------------------');
Log::i($tag, 'Cron start');
Log::i($tag, 'Action - Notify Auction');

try {
	$game = new Game();

	$game_server_date = $game->getServerDate();
	$hours = (int)$game_server_date->format('H');

	Log::i($tag, 'Game server time: ' . $game_server_date->format('d.m.Y H:i:s'));

	// auth
	$auth = $game->makeAuth();

	$names = [
		'You shot me',
		'so damn well',
		'Rampa-pa-ge',
		'You shot me then',
		'you catas me',
		"'nd I'm like damn",
		"Boo-booM don't",
		'def Rampa-pa-ge'
	];

	if ($auth) {
		$village_id = 52388;

		$name = $game->getVillageName($village_id);

		$key = array_search($name, $names);
		if (!is_integer($key)) {
			$key = 0;
		} else {
			$key++;
		}

		if ($key > count($names) - 1) {
			$key = 0;
		}

		$new_name = $names[$key];

		Log::i($tag, $name);
		Log::i($tag, $new_name);

		$game->changeVillageName($village_id, $new_name);
	}

} catch (Exception $e) {
	Log::w($tag, 'Message: ' . $e->getMessage());
}

Log::i($tag, 'Execute time: ' . round(microtime(true) - $start, 4) . ' sec.');